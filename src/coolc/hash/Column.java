/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package coolc.hash;

import coolc.ast.*;

/**
 *
 * @author sheila
 */
public class Column {
    
    public String dtype = "";
    public boolean init = false;
    public String typeScope = "";
    
    public String getDtype() {
        return this.dtype;
    }
    
    public void setDtype ( String dtype){
        this.dtype = dtype;
    }
    
    public boolean getInit(){
        return this.init;
    }
    
    public void setInit(boolean init){
        this.init = init;
    }
    
    public Column(String dtype, boolean init){
        this.dtype = dtype;
        this.init = init;
    }
    
    public Column(String dtype, boolean init, String typeScope){
        this.dtype = dtype;
        this.init = init;
        this.typeScope = typeScope;
    }
}
