/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package coolc.hash;

import java.util.*; 
/**
 *
 * @author sheila
 */
public class HashNode {

    public List<HashNode> children;
    public String tag = ""  ;
    public Hash hash;	

    public HashNode(Hash mph, String tag) {
        this.hash = mph;
        this.tag = tag;
        children = new ArrayList<HashNode>();
    }    

    /* zero ou mais AstNode podem sem passados*/	
    public HashNode(Hash hash, String tag,  HashNode... params) {
        this.hash = hash;
        this.tag = tag;
        for(HashNode o: params) {
		if(o != null){            
			children.add(o);}
        }
    }
}
    

