/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package coolc.hash;

import java.util.*; 

/**
 *
 * @author sheila
 */
public class Hash {
    
    public LinkedHashMap< String, Column > mp;

    public Hash( LinkedHashMap mp, String id, String dtype, boolean init){
        
        Column column = new Column(dtype, init);
        mp.put(id, column);
    }
    
    public Hash( String id, String dtype, boolean init){
        
        this.mp = new LinkedHashMap< String, Column >();
        Column column = new Column(dtype, init);
        mp.put(id, column);
    }
    
    public void addHash(String id, String dtype, boolean init, String typeScope){
        Column column = new Column(dtype, init, typeScope);
        this.mp.put(id, column);
    }
    
    public void addHash(String id, String dtype, boolean init){
        Column column = new Column(dtype, init);
        this.mp.put(id, column);
    }
    /*public String getKey(Map mp){
        return mp.getKey();
        
    }*/
    
    
}
