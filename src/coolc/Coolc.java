package coolc;

import coolc.parser.*;
import coolc.ast.*;
import coolc.hash.*;

import coolc.hash.HashNode;
import java.io.*;
import java.util.*;

public class Coolc {

    public static void printInstructions() {
        System.out.println(
            "Usage Coolc <action> file1 file2 ... filen\n" +
            "Available actions: \n" +
            "scan - scans each files and outputs a token list\n" +
            "parse - parses each file and outputs the syntax tree\n"
            );
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {

        if (args.length < 2) {
            printInstructions();
            return;
        }


        String action = args[0];

        List<String> files = Arrays.asList(args).subList(1, args.length);

        switch(action) {

            case "scan":
                scan(files);
                break;

            case "parse":
                parse(files);
                break;
                
            case "symtable":
                symtable(files);
                break;
            
            case "semantic":
                semantic(files);
                break;

            default:
                printInstructions();
                return;
        }


    }

    private static void scan(List<String> files) throws FileNotFoundException, IOException {

        for(String filename: files) {
            File f = new File(filename);
            Reader r = new FileReader(f);
            Lexer lex = new Lexer(r);
            Random rand = new Random();

            for(int token = lex.yylex(); token != Parser.EOF; token = lex.yylex()) {

                Position pos = lex.getStartPos();

                switch(token) {

                    case Parser.ID:
                        System.out.printf("%s:%2d:%2d  Id<%s>\n", f.getPath(), pos.line, pos.column, lex.getLVal());
                        break;

                    case Parser.INTEGER:
                        System.out.printf("%s:%2d:%2d  Int<%s>\n", f.getPath(), pos.line, pos.column, lex.getLVal());
                        break;
                        

                    case Parser.BOOL:
                        System.out.printf("%s:%2d:%2d  Boolean<%s>\n", f.getPath(), pos.line, pos.column, (boolean)lex.getLVal() ? "True" : "False");
                        break;
                        
                    case Parser.TYPE:
                        System.out.printf("%s:%2d:%2d  Type<%s>\n", f.getPath(), pos.line, pos.column, lex.getLVal());
                        break;

                    case Parser.STRING:
                        String strval =((String)lex.getLVal()).replace("\n","").replace("\b", "").replace("\t", "").replace(">", "");
                        strval = strval.substring(0, Math.min(strval.length(), 20));

                        System.out.printf("%s:%2d:%2d  String<%s>\n", f.getPath(), pos.line, pos.column, strval);
                        break;

                    default:
                        System.out.printf("%s:%2d:%2d  %s\n", f.getPath(), pos.line, pos.column, Parser.getTokenName(token));
                        break;
                }

            }
        }
    }
    
    private static void semantic(List<String> files) throws FileNotFoundException, IOException { 
        
            for(String filename: files) {
            File f = new File(filename);
            Reader r = new FileReader(f);
            Lexer lex = new Lexer(r);

            Parser p = new Parser(lex);

            System.err.printf("parsing %s\n", filename);

            p.parse();
            /* aqui monta a tabla de simbolo */
            HashNode hashnode = print_sym(p.getRoot());
            
            print(p.getRoot(), hashnode);
        }
    }

    private static void parse(List<String> files) throws FileNotFoundException, IOException { 



        for(String filename: files) {
            File f = new File(filename);
            Reader r = new FileReader(f);
            Lexer lex = new Lexer(r);

            Parser p = new Parser(lex);

            System.err.printf("parsing %s\n", filename);

            p.parse();
            /* aqui monta a tabla de simbolo */
           HashNode hashnode = print_sym(p.getRoot());
            
           print(p.getRoot(), hashnode);
        }

    }

    private static void print(Program root, HashNode hashnode) {
        System.out.println("program");
        /***********************************************************************/
            /*        Iterator<String> keyIterator = c.hash.mp.keySet().iterator();
            String firstKey = keyIterator.next();
            printIndent(i);
            System.out.println("class " + firstKey + " : " + c.hash.mp.get(firstKey).getDtype());
            printIndent(i+1);
            System.out.println("fields");

            while (keyIterator.hasNext()) {
                String code = keyIterator.next();
                if (!code.equals(firstKey)){
                    printIndent(i+2);
                    System.out.println(c.hash.mp.get(code).dtype + " " + code);
                }
            }*/

        
        /***********************************************************************/
        for(ClassDef c: root) {
            for(HashNode f: hashnode.children){
                if (f.hash.mp.containsKey(c.getType())) {
                    print(c, f);
                }
            }
        }
    }

    private static void print(ClassDef c, HashNode hashnode) {
        printIndent(1);
        System.out.printf("class %s", c.getType());
        if( c.getSuper() != null ) {
            System.out.printf(" : %s", c.getSuper());
        }
        System.out.println();

        for(Feature f: c.getBody()) {
            print(f, hashnode);
        }
    }

    private static void print(Feature f, HashNode hashnode) {
        if(f instanceof Method) {
            
            Method m = (Method)f;


            printIndent(2);
            System.out.printf("method %s : ", m.getName());
            for(Variable var: m.getParams()) {
                System.out.printf("%s %s -> ", var.getType(), var.getId());

                if( var.getValue() != null ){
                    throw new RuntimeException("WTF? initializing parameter definition?");
                }
            }
            System.out.println(m.getType());

            //for(HashNode h: hashnode.children){
                //if(h.hash.mp.containsKey(m.getName())){
            print(m.getBody(), 3, hashnode);
                //}
                    
            //}
            

        }
        else if (f instanceof Variable) {
            Variable var = (Variable)f;
            printIndent(2);
            System.out.printf("field %s %s\n", var.getType(), var.getId());
            if( var.getValue() != null ) {
                //for(HashNode h: hashnode.children){
                    //if(h.hash.mp.containsKey(var.getId())){
                        print(var.getValue(), 3, hashnode);
                //}
                    
            //}
                
            }
        }
        else {
            throw new RuntimeException("Unknown feature type " + f.getClass());
        }
    }

    private static void printIndent(int indent) {
        for (int i = indent; i > 0 ; i-- ) {
            System.out.print("    ");
        }  
    }
    
    @SuppressWarnings("unchecked")
    private static String print(Expr e, int indent, HashNode hashnode) {

        assert e != null : "node is null";
        String result = "";
        printIndent(indent);

        if(e instanceof Block) {
            System.out.println("block [" + printr(((Block)e).getStatements().get(((Block)e).getStatements().size()-1)) + "]");
            
            for(Expr child: ((Block)e).getStatements() ) {               
                print(child, indent+1, hashnode);
            }
        }
        else if(e instanceof WhileExpr) {
            WhileExpr loop = (WhileExpr)e;
            
            if(printr(loop.getCond()).equals("Bool"))
                System.out.println("while [Object]");
            else
                System.out.println("while ERROR");
            print(loop.getCond(), indent+1, hashnode);

            printIndent(indent);
            System.out.println("loop");
            print(loop.getValue(), indent+1, hashnode);


        }
        else if(e instanceof AssignExpr) {
            //if (((AssignExpr)e).getId().equals(printr(((AssignExpr)e).getValue())))
            System.out.printf("assign %s\n", ((AssignExpr)e).getId());
            print(((AssignExpr)e).getValue(), indent+1, hashnode);
        }        
        else if(e instanceof DispatchExpr) {
            DispatchExpr call = (DispatchExpr)e;

            System.out.printf("call %s", call.getName());
            if(call.getType() != null) {
                System.out.printf(" as %s", call.getType());
            }
            System.out.println();
            if( call.getExpr() != null ) {
                printIndent(indent+1);
                System.out.println("callee");
                print(call.getExpr(), indent+2, hashnode);
            }
            if (call.getArgs().size() > 0) {
                printIndent(indent+1);
                System.out.println("args");
                for(Expr arg: call.getArgs()) {
                    print(arg, indent+2, hashnode);
                }
            }
        }
        else if(e instanceof IfExpr) {
            IfExpr cond = (IfExpr)e;
            
            if(printr(cond.getCond()).equals("Bool"))
                System.out.println("if");
            else 
                System.out.println("if ERROR");
            print(cond.getCond(), indent+1, hashnode);

            printIndent(indent);
            System.out.println("then");
            print(cond.getTrue(), indent+1, hashnode);

            printIndent(indent);
            System.out.println("else");
            print(cond.getFalse(), indent+1, hashnode);

        }
        else if(e instanceof NewExpr) 
        {
            System.out.printf("new %s\n",((NewExpr)e).getType());
        }
        else if(e instanceof UnaryExpr) {
            UnaryExpr expr = (UnaryExpr)e;
            System.out.printf("unary %s ", operator(expr.getOp()));
            
            if(operator(expr.getOp()).equals("~") && printr(expr.getValue()).equals("Int"))
                System.out.printf("[Int] \n");
            else if (operator(expr.getOp()).equals("not") && printr(expr.getValue()).equals("Bool"))
                System.out.printf("[Bool] \n");
            else
                System.out.printf("ERROR \n");
            print(expr.getValue(), indent + 1, hashnode);
        }
        else if(e instanceof BinaryExpr) {
            BinaryExpr expr = (BinaryExpr)e;
            if(printr(expr.getLeft()).equals(printr(expr.getRight())))
                System.out.printf("binary %s [%s]\n", operator(expr.getOp()), printr(expr.getLeft()));
            else
                System.out.printf("binary %s ERROR\n", operator(expr.getOp()), printr(expr.getLeft()));
            print(expr.getLeft(), indent + 1, hashnode);   
            print(expr.getRight(), indent + 1, hashnode);   
        }
        else if (e instanceof CaseExpr) {
            CaseExpr caseExpr = ((CaseExpr)e);
            System.out.println("instanceof");
            print(caseExpr.getValue(), indent+1, hashnode);

            for(Case c : caseExpr.getCases()) {
                printIndent(indent+1);
                System.out.printf("case %s %s\n", c.getType(), c.getId());
                print(c.getValue(), indent+2, hashnode);
            }

        }
        else if (e instanceof LetExpr) {
            LetExpr let = (LetExpr)e;
            System.out.println("let");
            printIndent(indent+1);
            System.out.println("vars");
            for(Variable var : let.getDecl()) {
                printIndent(indent+2);
                System.out.printf("%s %s\n", var.getType(), var.getId());
                if(var.getValue() != null) {
                    print(var.getValue(), indent+3, hashnode);
                }
            }

            print(let.getValue(), indent+1, hashnode);
        }
        else if(e instanceof IdExpr) {
            
            /* Verificar si variable ya fue declarada*/
            System.out.printf("id %s ", ((IdExpr)e).getId());
            boolean attribute = true;
            
            if(((IdExpr)e).getId().equals("self")){
                /*primero elemento*/
                Iterator<String> keyIterator;
                keyIterator = hashnode.hash.mp.keySet().iterator();
                String firstKey = keyIterator.next();
                System.out.printf("[" + firstKey + "]\n");
            }
            else{
                Iterator<String> keyIterator = null;
               
                for(HashNode teste: hashnode.children){
                    if(teste instanceof HashNode){
                        
                    keyIterator = teste.hash.mp.keySet().iterator();
                    
                    String firstKey = keyIterator.next();
                    if(firstKey.equals(((IdExpr)e).getId())){
                        System.out.printf("[" + teste.hash.mp.get(firstKey).dtype + "]\n");
                        attribute = false;
                      
                    }
                    }
                }
                if (attribute)
                    System.out.printf("\n"); //ERROR
            
            }        //else
                //System.out.printf("\n");
            
        }
        else if(e instanceof ValueExpr) {
            Object value = ((ValueExpr)e).getValue();

            if(value instanceof String) {
                System.out.printf("str \"%s\" [String]\n", ((String)value).replace("\n", "\\n")
                    .replace("\t", "\\t").replace("\f", "\\f").replace("\b", "\\b"));
                result = "String";
            }
            else if(value instanceof Integer) {
                System.out.printf("int %d [Int]\n", value);
                result = "Int";
            }
            else if(value instanceof Boolean) {
                System.out.printf("bool %s [Bool]\n", value);
                result = "Bool";
            }
            else {
                throw new RuntimeException(String.format("Unsupported constant type %s\n", e.getClass()));
            }
        }
        else {

            if( e != null) {
                throw new RuntimeException(String.format("Unsupported node type %s\n", e.getClass()));
            }
            else {
                throw new RuntimeException(String.format("Null node", e.getClass()));
            }

        }
        return result;

    }
/********************************** ENTREGA 3 ANALISE SEMANTICA ***************************/
     private static String printr(Expr e) {

        assert e != null : "node is null";
        String result = "";
        //printIndent(indent);

        if(e instanceof Block) {
            //System.out.println("block");
            
            for(Expr child: ((Block)e).getStatements() ) {
                printr(child);
            }
        }
        else if(e instanceof WhileExpr) {
            WhileExpr loop = (WhileExpr)e;

            //System.out.println("while");
            printr(loop.getCond());

            //printIndent(indent);
            //System.out.println("loop");
            printr(loop.getValue());


        }
        else if(e instanceof AssignExpr) {
            //System.out.printf("assign %s\n", ((AssignExpr)e).getId());
            printr(((AssignExpr)e).getValue());
        }        
        else if(e instanceof DispatchExpr) {
            DispatchExpr call = (DispatchExpr)e;

            //System.out.printf("call %s", call.getName());
            if(call.getType() != null) {
                //System.out.printf(" as %s", call.getType());
            }
            //System.out.println();
            if( call.getExpr() != null ) {
                //printIndent(indent+1);
                //System.out.println("callee");
                printr(call.getExpr());
            }
            if (call.getArgs().size() > 0) {
                //printIndent(indent+1);
                //System.out.println("args");
                for(Expr arg: call.getArgs()) {
                    printr(arg);
                }
            }
        }
        else if(e instanceof IfExpr) {
            IfExpr cond = (IfExpr)e;

            //System.out.println("if");
            printr(cond.getCond());

            //printIndent(indent);
            //System.out.println("then");
            printr(cond.getTrue());

            //printIndent(indent);
            //System.out.println("else");
            printr(cond.getFalse());

        }
        else if(e instanceof NewExpr) 
        {
            //System.out.printf("new %s\n",((NewExpr)e).getType());
        }
        else if(e instanceof UnaryExpr) {
            UnaryExpr expr = (UnaryExpr)e;
            //System.out.printf("unary %s\n", operator(expr.getOp()));
            printr(expr.getValue());
        }
        else if(e instanceof BinaryExpr) {
            BinaryExpr expr = (BinaryExpr)e;
            //System.out.printf("binary %s\n", operator(expr.getOp()));
            printr(expr.getLeft());   
            printr(expr.getRight());   
        }
        else if (e instanceof CaseExpr) {
            CaseExpr caseExpr = ((CaseExpr)e);
            //System.out.println("instanceof");
            printr(caseExpr.getValue());

            for(Case c : caseExpr.getCases()) {
                //printIndent(indent+1);
                //System.out.printf("case %s %s\n", c.getType(), c.getId());
                printr(c.getValue());
            }

        }
        else if (e instanceof LetExpr) {
            LetExpr let = (LetExpr)e;
            //System.out.println("let");
            //printIndent(indent+1);
            //System.out.println("vars");
            for(Variable var : let.getDecl()) {
                //printIndent(indent+2);
                //System.out.printf("%s %s\n", var.getType(), var.getId());
                if(var.getValue() != null) {
                    printr(var.getValue());
                }
            }

            printr(let.getValue());
        }
        else if(e instanceof IdExpr) {
            //System.out.printf("id %s\n", ((IdExpr)e).getId());
            result = ((IdExpr)e).getId();
        }
        else if(e instanceof ValueExpr) {
            Object value = ((ValueExpr)e).getValue();

            if(value instanceof String) {
                //System.out.printf("str \"%s\" [String]\n", ((String)value).replace("\n", "\\n")
                   // .replace("\t", "\\t").replace("\f", "\\f").replace("\b", "\\b"));
                result = "String";
            }
            else if(value instanceof Integer) {
                //System.out.printf("int %d [Int]\n", value);
                result = "Int";
            }
            else if(value instanceof Boolean) {
                //System.out.printf("bool %s [Bool]\n", value);
                result = "Bool";
            }
            else {
                throw new RuntimeException(String.format("Unsupported constant type %s\n", e.getClass()));
            }
        }
        else {

            if( e != null) {
                throw new RuntimeException(String.format("Unsupported node type %s\n", e.getClass()));
            }
            else {
                throw new RuntimeException(String.format("Null node", e.getClass()));
            }

        }
        return result;

    }
    

 /*****************************************************************************************/   

    public static String operator(UnaryOp op) {
        switch(op) {
            case ISVOID:    return "isvoid";
            case NEGATE:    return "~"; 
            case NOT:       return "not";
        }

        return null;
    }


    public static String operator(BinaryOp op) {
        switch(op) {
            case PLUS:      return "+";
            case MINUS:     return "-";
            case MULT:      return "*";
            case DIV:       return "/";            
            case LT:        return "<";
            case LTE:       return "<=";
            case EQUALS:    return "=";
        }
        return null;
    }

/********** ENTREGA 3 **********/
 private static void symtable(List<String> files) throws FileNotFoundException, IOException { 



        for(String filename: files) {
            File f = new File(filename);
            Reader r = new FileReader(f);
            Lexer lex = new Lexer(r);

            Parser p = new Parser(lex);

            System.err.printf("parsing %s\n", filename);

            p.parse();

            HashNode hashnode = print_sym(p.getRoot());
            
            print_tree(hashnode,0);
        }

    }
 
    private static void print_tree(HashNode hashnode, int i){
        //printIndent(i);
        //System.out.println(hashnode.tag);
        
        /********* CLASSNODE *****************/
        if(hashnode instanceof ClassHash){
            ClassHash c = (ClassHash)hashnode;
            //String firstKey = c.hash.mp.firstKey();
            Iterator<String> keyIterator = c.hash.mp.keySet().iterator();
            String firstKey = keyIterator.next();
            printIndent(i);
            System.out.println("class " + firstKey + " : " + c.hash.mp.get(firstKey).getDtype());
            printIndent(i+1);
            System.out.println("fields");

            while (keyIterator.hasNext()) {
                String code = keyIterator.next();
                if (!code.equals(firstKey)){
                    printIndent(i+2);
                    System.out.println(c.hash.mp.get(code).dtype + " " + code);
                }
            }

           
            for(HashNode f: c.children){
                if (f instanceof ValueHash){
                    ValueHash g = (ValueHash)f;
                    print_tree(g, i+2);
                }
            }    
               
            printIndent(i+1);
            System.out.println("methods");
            
            for(HashNode f: c.children){
                if (f instanceof MethodHash){
                    MethodHash g = (MethodHash)f;
                    print_tree(g, i+2);
                }
            } 
            
        }
        /********** FIM CLASSNODE **************/
        
        /********** VALUEHASH *****************/
        else if(hashnode instanceof ValueHash){
            ValueHash c = (ValueHash)hashnode;
            
            Iterator<String> keyIterator = c.hash.mp.keySet().iterator();
            String firstKey = keyIterator.next();
            
            printIndent(i);
            System.out.println(c.hash.mp.get(firstKey).getDtype() + " " + firstKey);

            for(HashNode f: c.children){
                print_tree(f, i+1);
            }
            
        }
        /********* FIM VALUEHASH **************/
        
        
        /********** METHODNODE *****************/
        else if(hashnode instanceof MethodHash){
            MethodHash c = (MethodHash)hashnode;
            //String firstKey = c.hash.mp.firstKey();
            Iterator<String> keyIterator = c.hash.mp.keySet().iterator();
            String firstKey = keyIterator.next();
            
            //printIndent(i);
            //System.out.println("methods");
            printIndent(i);
            System.out.println( c.hash.mp.get(firstKey).getDtype() + " " + firstKey);
            
            boolean passed = true;
            while (keyIterator.hasNext()) {
                String code = keyIterator.next();
                if (!code.equals(firstKey)){
                    
                    if (passed){
                        printIndent(i+1);
                        System.out.println("arguments");
                        passed = false;
                    }
                    
                    printIndent(i+2);
                    System.out.println(c.hash.mp.get(code).dtype + " " + code);
                }
            }

            printIndent(i+1);
            System.out.println("body");
            for(HashNode f: c.children){
                print_tree(f, i+2);
            }
            
        }
        
        
        
        /********** FIM METHODNODE *****************/
        
        /********** CASENODE *****************/
        else if(hashnode instanceof CaseHash){
            CaseHash c = (CaseHash)hashnode;
            //String firstKey = c.hash.mp.firstKey();
            Iterator<String> keyIterator = c.hash.mp.keySet().iterator();
            String firstKey = keyIterator.next();

            printIndent(i);
            System.out.println("scope");
            printIndent(i+1);
            System.out.println(c.hash.mp.get(firstKey).getDtype() + " " + firstKey);

            for(HashNode f: c.children){
                print_tree(f, i+2);
            }
            
        }  
        
        /********** FIM CASENODE *****************/
        
        /********** LETHASH *****************/
        else if(hashnode instanceof LetHash){
            LetHash c = (LetHash)hashnode;
            Iterator<String> keyIterator = c.hash.mp.keySet().iterator();

            //printIndent(i);
            //System.out.println("scope");

            while (keyIterator.hasNext()) {
                String code = keyIterator.next();
                printIndent(i);
                System.out.println(c.hash.mp.get(code).dtype + " " + code);
                
            }


            for(HashNode f: c.children){
                print_tree(f, i+1);
            }
            
        }
        
            
        /********** FIM LETHASH *****************/
        
       /********** LETHASH 2 *****************/
        else if(hashnode instanceof LetHash2){
            LetHash2 c = (LetHash2)hashnode;
            //Iterator<String> keyIterator = c.hash.mp.keySet().iterator();

            printIndent(i);
            System.out.println("scope");


            for(HashNode f: c.children){
                print_tree(f, i+1);
            }
            
        }
        
            
        /********** FIM LETHASH *****************/
        else {

            Iterator<String> keyIterator = hashnode.hash.mp.keySet().iterator();
            while (keyIterator.hasNext()) {
                String code = keyIterator.next();
                //printIndent(i);
                //System.out.println(code);
            }
            for(HashNode c: hashnode.children){
                print_tree(c, i);
            }
        }
    }
    

    private static HashNode print_sym(Program root) {
        HashNode hashnode = new HashNode(new Hash("program", "program", true), "program");
        //System.out.println(hashnode.hash.mp.get("program").getDtype());
        for(ClassDef c: root) {            
            print_sym_map(c, hashnode);
        }
        return hashnode;
    }

    private static void print_sym_map(ClassDef c, HashNode hashnode) {
        //printIndent(1);
        //System.out.printf("class %s", c.getType());
        ClassHash class_hash = null;
        
        if( c.getSuper() != null ) {  
            class_hash = new ClassHash(new Hash(c.getType(),c.getSuper(), true), "class");
            //System.out.printf(" : %s", c.getSuper());
        }
        else 
            class_hash = new ClassHash(new Hash(c.getType(),"Object", true), "class");
        
        hashnode.children.add(class_hash);
        //System.out.println("class " + c.getType() + " : " + hashnode.hash.mp.get(c.getType()).getDtype());
        for(Feature f: c.getBody()) {
            print_sym_map(f, class_hash);
        }
    }

    private static void print_sym_map(Feature f, ClassHash class_hash) {
        if(f instanceof Method) {
            //printIndent(1);
            Method m = (Method)f;
            //System.out.println("SHEILAAAAAAAAAAAAAA " + m.getName());
            MethodHash method_hash = null;
            method_hash = new MethodHash(new Hash(m.getName(),m.getType() , true), "method");
            //System.out.printf("method %s : ", m.getName());
            for(Variable var: m.getParams()) {
                //System.out.printf("%s %s -> ", var.getType(), var.getId());
                if( var.getValue() != null ){
                    throw new RuntimeException("WTF? initializing parameter definition?");
                }
                else {
                    method_hash.hash.addHash(var.getId(), var.getType(), true, "functionAttr");
                }
            }
            //System.out.println(m.getType());

            //print_sym(m.getBody(), 3);
            class_hash.children.add(method_hash);
            print_sym_map(m.getBody(), method_hash);

        }
        else if (f instanceof Variable) {
            Variable var = (Variable)f;
            //printIndent(2);
            //System.out.printf("field %s %s\n", var.getType(), var.getId());
            //System.out.println("SHEILAAAAAAAAAAAAAA " + var.getId());
            if( var.getValue() != null ) {
                //class_hash.hash.addHash(var.getId(), var.getType(), true);
                //print_sym(var.getValue(), 3);
                ValueHash valuehash = new ValueHash(new Hash(var.getId(), var.getType(), true), "attribute");
                //System.out.println("SHEILAAAAAAAAAAAA");
                class_hash.children.add(valuehash);
                print_sym_map(var.getValue(), valuehash);
            }
            else {
                //class_hash.hash.addHash(var.getId(), var.getType(), false);
                ValueHash valuehash = new ValueHash(new Hash(var.getId(), var.getType(), true), "attribute");
                class_hash.children.add(valuehash);
                
            }
        }
        else {
            throw new RuntimeException("Unknown feature type " + f.getClass());
        }
    }

    @SuppressWarnings("unchecked")
    //private static void print_sym_map(Expr e, int indent) {
    private static void print_sym_map(Expr e, HashNode hashnode) {    
        assert e != null : "node is null";

        //printIndent(indent);

        if(e instanceof Block) {
            //System.out.println("block");
            for(Expr child: ((Block)e).getStatements()) {
                //print_sym(child, indent+1);
                print_sym_map(child, hashnode);
            }
        }
        else if(e instanceof WhileExpr) {
            WhileExpr loop = (WhileExpr)e;

            //System.out.println("while");
            //print_sym(loop.getCond(), indent+1);
            print_sym_map(loop.getCond(), hashnode);
            //printIndent(indent);
            //System.out.println("loop");
            //print_sym(loop.getValue(), indent+1);
            print_sym_map(loop.getValue(), hashnode);

        }
        else if(e instanceof AssignExpr) {
            // AQUI VIRA TRUE QUE FOI INICIADO, NA ANALISE SEMANTICA
            //System.out.printf("assign %s\n", ((AssignExpr)e).getId());
            //print_sym(((AssignExpr)e).getValue(), indent+1);
            print_sym_map(((AssignExpr)e).getValue(), hashnode);
        }        
        else if(e instanceof DispatchExpr) {
            DispatchExpr call = (DispatchExpr)e;

            //System.out.printf("call %s", call.getName());
            //if(call.getType() != null) {
              //  System.out.printf(" as %s", call.getType());
            //}
            //System.out.println();
            if( call.getExpr() != null ) {
                //printIndent(indent+1);
                //System.out.println("callee");
                //print_sym(call.getExpr(), indent+2);
                print_sym_map(call.getExpr(), hashnode);
            }
            if (call.getArgs().size() > 0) {
                //printIndent(indent+1);
                //System.out.println("args");
                for(Expr arg: call.getArgs()) {
                    //print_sym(arg, indent+2);
                    print_sym_map(arg, hashnode);
                }
            }
        }
        else if(e instanceof IfExpr) {
            IfExpr cond = (IfExpr)e;

            //System.out.println("if");
            //print_sym(cond.getCond(), indent+1);
            print_sym_map(cond.getCond(), hashnode);

            //printIndent(indent);
            //System.out.println("then");
            //print_sym(cond.getTrue(), indent+1);
            print_sym_map(cond.getTrue(), hashnode);

            //printIndent(indent);
            //System.out.println("else");
            //print_sym(cond.getFalse(), indent+1);
            print_sym_map(cond.getFalse(), hashnode);

        }
        else if(e instanceof NewExpr) 
        {
            //System.out.printf("new %s\n",((NewExpr)e).getType());
        }
        else if(e instanceof UnaryExpr) {
            UnaryExpr expr = (UnaryExpr)e;
            //System.out.printf("unary %s\n", operator(expr.getOp()));
            //print_sym(expr.getValue(), indent + 1);
            print_sym_map(expr.getValue(), hashnode);
        }
        else if(e instanceof BinaryExpr) {
            BinaryExpr expr = (BinaryExpr)e;
            //System.out.printf("binary %s\n", operator(expr.getOp()));
            //print_sym(expr.getLeft(), indent + 1);
            print_sym_map(expr.getLeft(), hashnode);
            //print_sym(expr.getRight(), indent + 1);
            print_sym_map(expr.getRight(), hashnode);   
        }
        else if (e instanceof CaseExpr) {
            CaseExpr caseExpr = ((CaseExpr)e);
            //System.out.println("instanceof");
            //print_sym(caseExpr.getValue(), indent+1);
            print_sym_map(caseExpr.getValue(), hashnode);

            for(Case c : caseExpr.getCases()) {
                //printIndent(indent+1);
                //System.out.printf("case %s %s\n", c.getType(), c.getId());
                CaseHash case_hash =  new CaseHash(new Hash(c.getId(),c.getType(),false),"scope");
                hashnode.children.add(case_hash);
                //print_sym(c.getValue(), indent+2);
                print_sym_map(c.getValue(), case_hash);
            }

        }
        else if (e instanceof LetExpr) {
            LetExpr let = (LetExpr)e;
            //System.out.println("let");
            //printIndent(indent+1);
            //System.out.println("vars");
            LetHash2 let_hash2 = new LetHash2(new Hash("let","let",true), "scope");
           
            LetHash let_hash = null;
            //boolean i = true;
            for(Variable var : let.getDecl()) {
                //printIndent(indent+2);
                //System.out.printf("%s %s\n", var.getType(), var.getId());
                
                if(var.getValue() != null) {
                    //if (i){
                       let_hash = new LetHash(new Hash(var.getId(),var.getType(),true), "scope");
                       //i = false;
                    /*}
                    else {
                        let_hash.hash.addHash(var.getId(), var.getType(), false);
                    }*/
                    //print_sym(var.getValue(), indent+3);
                    print_sym_map(var.getValue(), let_hash);
                }
                else
                    //if(i){
                        let_hash = new LetHash(new Hash(var.getId(),var.getType(),false), "scope");
                        //i= false;
                    /*}
                    else{
                        let_hash.hash.addHash(var.getId(), var.getType(), false);
                    }*/
                let_hash2.children.add(let_hash);
            }

            hashnode.children.add(let_hash2);
            //print_sym(let.getValue(), indent+1);
            print_sym_map(let.getValue(), let_hash2);
        }
        else if(e instanceof IdExpr) {
            //System.out.printf("id %s\n", ((IdExpr)e).getId());
        }
        else if(e instanceof ValueExpr) {
            Object value = ((ValueExpr)e).getValue();

            if(value instanceof String) {
                //System.out.printf("str \"%s\"\n", ((String)value).replace("\n", "\\n")
                  //  .replace("\t", "\\t").replace("\f", "\\f").replace("\b", "\\b"));
            }
            else if(value instanceof Integer) {
                //System.out.printf("int %d\n", value);
            }
            else if(value instanceof Boolean) {
                //System.out.printf("bool %s\n", value);
            }
            else {
                throw new RuntimeException(String.format("Unsupported constant type %s\n", e.getClass()));
            }
        }
        else {

            if( e != null) {
                throw new RuntimeException(String.format("Unsupported node type %s\n", e.getClass()));
            }
            else {
                throw new RuntimeException(String.format("Null node", e.getClass()));
            }

        }

    }

/********** FIN ENTREGA 3 *****/




}  
